;*******************************************************************
; Function:  Listens for commands over RS232, and controls motors with PWM.
; Processor: PIC16F628 at 4 MHz using internal RC oscillator
; Filename:  merakibot.asm
; Author:    Andrew Walbran
; Pin assignments:
;   9 (RB3/CCP1) is PWM output, for H-bridge enable
;   17-18 (RA0-RA1) are H-bridge 1 forward / reverse
;   1-2 (RA2-RA3) are H-bridge 2 forward / reverse
;*******************************************************************

        LIST P=16F628A, R=DEC    ; Use the PIC16F628 and decimal system

        #include "p16f628a.inc"  ; Include header file

        __config  _INTRC_OSC_NOCLKOUT & _LVP_OFF & _WDT_OFF & _PWRTE_ON & _BODEN_OFF & _MCLRE_OFF

        CBLOCK 0x20             ; Declare variable addresses starting at 0x20
          dataL
          command
        ENDC

        ORG    0x000            ; Program starts at 0x000

; ----------------
; INITIALIZE PORTS
; ----------------
        movlw 7
        movwf CMCON             ; CMCON=7 set comperators off

        movlw b'00000000'       ; set up portA
        movwf PORTA

        movlw b'00000100'       ; RB2(TX)=1 others are 0
        movwf PORTB

        ;Setup PWM
        movlw 0x77
        movwf CCPR1L            ; Initial duty cycle
        bcf CCP1CON,5
        bcf CCP1CON,4
        movlw b'00000110'
        movwf T2CON             ; Enable timer2 and set prescale value to 1:16 (leave postscale at 1:1)
        movlw b'00001100'
        movwf CCP1CON           ; Enable PWM mode, and set two LSbs of duty cycle to 00

        bsf STATUS,RP0          ; RAM PAGE 1

        movlw 0xFF
        movwf PR2               ; PWM frequency (note that this is in RAM page 1)

        movlw 0xF0
        movwf TRISA             ; RA7-4 input, RA3-0 output

        movlw b'11110011'       ; RB3/CCP1=output, RB2(TX)=output, RB1(RX)=input, others input
        movwf TRISB

; ------------------------------------
; SET BAUD RATE TO COMMUNICATE WITH PC
; ------------------------------------
; Boot Baud Rate = 9600, No Parity, 1 Stop Bit
        movlw 0x19              ; 0x19=9600 bps (0x0C=19200 bps)
        movwf SPBRG
        movlw b'00100100'       ; brgh = high (2)
        movwf TXSTA             ; enable Async Transmission, set brgh

        bcf STATUS,RP0          ; RAM PAGE 0

        movlw b'10010000'       ; enable Async Reception
        movwf RCSTA

; ------------------------------------
; PROVIDE A SETTLING TIME FOR START UP
; ------------------------------------
        clrf dataL
settle  decfsz dataL,F
        goto settle

        movf RCREG,W
        movf RCREG,W
        movf RCREG,W            ; flush receive buffer

; ---------
; MAIN LOOP
; ---------
loop    call receive            ; wait for a byte to select the command
        movwf command
        call send
        ;Switch based on command byte
        movlw 'O'
        xorwf command,W
        btfsc STATUS,Z
        goto outputCommand
        movlw 'P'
        xorwf command,W
        btfsc STATUS,Z
        goto pwmCommand
        goto invalidCommand

outputCommand                   ; Write the next byte received out to PORTA
        call receive
        movwf PORTA
        movlw 'O'
        call send
        movlw 'u'
        call send
        movlw 't'
        call send
        movlw 'p'
        call send
        movlw 'u'
        call send
        movlw 't'
        call send
        movlw 0x0D ; CR
        call send
        movlw 0x0A ; LF
        call send
        goto loop

pwmCommand
        call receive
        movwf CCPR1L            ; Set duty cycle
        movlw 'P'
        call send
        movlw 'W'
        call send
        movlw 'M'
        call send
        movlw 0x0D ; CR
        call send
        movlw 0x0A ; LF
        call send
        goto loop

invalidCommand
        movlw 'I'
        call send
        movlw 'n'
        call send
        movlw 'v'
        call send
        movlw 'a'
        call send
        movlw 'l'
        call send
        movlw 'i'
        call send
        movlw 'd'
        call send
        movlw ' '
        call send
        movlw 'c'
        call send
        movlw 'o'
        call send
        movlw 'm'
        call send
        movlw 'm'
        call send
        movlw 'a'
        call send
        movlw 'n'
        call send
        movlw 'd'
        call send
        movlw 0x0D ; CR
        call send
        movlw 0x0A ; LF
        call send
        goto loop

; -------------------------------------------
; RECEIVE CHARACTER FROM RS232 AND STORE IN W
; -------------------------------------------
; This routine does not return until a character is received.
receive btfss PIR1,RCIF         ; (5) check for received data
        goto receive

        movf RCREG,W            ; save received data in W
        return

; -------------------------------------------------------------
; SEND CHARACTER IN W VIA RS232 AND WAIT UNTIL FINISHED SENDING
; -------------------------------------------------------------
send    movwf TXREG             ; send data in W

TransWt bsf STATUS,RP0          ; RAM PAGE 1
WtHere  btfss TXSTA,TRMT        ; (1) transmission is complete if hi
        goto WtHere

        bcf STATUS,RP0          ; RAM PAGE 0
        return

        END
